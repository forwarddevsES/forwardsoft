<?php

return [
    'welcome'                => 'Bienvenido al Compás de ForwardSoft. Toda buena aplicación necesita una brújula para señalarlos en la dirección correcta.
    En esta sección encontrará muchos recursos y tareas administrativas que lo ayudarán a guiarlo a medida que desarrolla su aplicación.',
    'links'         => [
        'title'                 => 'Links',
        'documentation'         => 'Documentación de Voyager',
        'voyager_homepage'      => 'Web Voyager',
        'voyager_hooks'         => 'Voyager Hooks',
    ],
    'commands'      => [
        'title'                 => 'Comandos',
        'text'                  => 'Ejecute comandos de laravel.',
        'clear_output'          => 'limpiar consola',
        'command_output'        => 'Consola de Salida',
        'additional_args'       => '¿Argumentos adicionales?',
        'run_command'           => 'Ejecutar Comando',
    ],
    'resources'     => [
        'title'                 => 'Recursos',
        'text'                  => 'Voyager resources to help you find things quicker.',

    ],
    'logs'          => [
        'title'                 => 'Logs',
        'text'                  => 'Your app logs',
        'file_too_big'          => 'Log file >50M, please download it.',
        'level'                 => 'Level',
        'context'               => 'Context',
        'date'                  => 'Date',
        'content'               => 'Content',
        'download_file'         => 'Download file',
        'delete_file'           => 'Delete file',
        'delete_all_files'      => 'Delete all files',
        'delete_success'        => 'Successfully deleted log file:',
        'delete_all_success'    => 'Successfully deleted all log files',

    ],
    'fonts'         => [
        'title'                 => 'Fonts',
        'font_class'            => 'Voyager Fonts Class Mapping',
        'font_character'        => 'Voyager Fonts Character Mapping',
    ],
];
